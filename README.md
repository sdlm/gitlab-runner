# Gitlab Runner Template

Most important part is:
```
image = "docker:latest"
host = "unix:///var/run/docker.sock"
disable_entrypoint_overwrite = true
volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
privileged = true
```
